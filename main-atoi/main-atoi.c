#include <assert.h>
#include "stdio.h"

// FW declaration
int myatoibase(const char *nptr, int base);

void testing() {
    assert(myatoibase("128", 10) == 128);
    assert(myatoibase("-aac", 13) == -1832);
    assert(myatoibase("112", 4) == 22);
    assert(myatoibase(NULL, 4) == 0);
    assert(myatoibase("a", 10) == 10);
    assert(myatoibase("0a876", 8) == -1);
    assert(myatoibase("-0z8zzz556", 8) == -1);
    assert(myatoibase("128", 38) == -2);
    assert(myatoibase("128", 1) == -2);
    assert(myatoibase("!", 10) == 36);
    assert(myatoibase("", 10) == -1);
    assert(myatoibase("-!!!", 37) == -50652);
}

// Test main
int main(void) {
    // My code
    testing();
    // Hochart code
    int res;
    int cmp;

    // Test 1
    res = myatoibase("128", 10);
    cmp = (res == 128);
    printf("Test 1: Pass=%d, (%d)\n", cmp, res);

    // Test 2
    res = myatoibase("-aac", 13);
    cmp = (res == -1832);
    printf("Test 2: Pass=%d, (%d)\n", cmp, res);

    // Ret
    return (0);
}