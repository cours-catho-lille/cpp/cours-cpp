#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int puissance(int a, int b) {
    int result = 1;
    for (int i = 0; i < b; i++) {
        result = result * a;
    }
    return result;
}

int myatoibase(char *nptr, const int base) {
    if (nptr == NULL) {
        return 0;
    }
    if (strcmp(nptr, "") == 0) {
        return -1;
    }
    if (nptr[0] == '-') {
        if (nptr[1] == '0') {
            return -1;
        }
    } else if (nptr[0] == '0') {
        return -1;
    }
    if (base < 2 || base > 37) {
        return -2;
    }

    int incr = strlen(nptr);
    int result = 0;
    for (int i = 0; i < strlen(nptr); i++) {
        if (i != 0 && nptr[i] == '-') {
            return -1;
        }
        int puissancea = puissance(base, --incr);

        if (nptr[i] >= 48 && nptr[i] <= 57) {
            result += (nptr[i] - 48) * puissancea;
        } else if (nptr[i] >= 97 && nptr[i] <= 122) {
            result += (nptr[i] - 87) * puissancea;
        } else if (nptr[i] == 33) {
            result += 36 * puissancea;
        } else {
            if (i != 0) {
                return -1;
            }
        }
    }

    if (nptr[0] == '-') {
        result = result * -1;
    }
    return result;
}