cmake_minimum_required(VERSION 3.12)
project(mychainedstack)

set(CMAKE_CXX_STANDARD 11)

add_executable(scrabble myfastscrabble.cpp)
