//
// Created by artandor on 05/10/18.
//

#include <iostream>
#include "mychainedstack.h"

void Mychainedstack::push(int number) {
    container newEntry = container{number, nullptr};
    if (listLen == 0) {
        list[listLen] = newEntry;
    } else {
        list[listLen] = newEntry;
        list[listLen - 1].nextCont = &newEntry;
    }
    listLen++;
}

int Mychainedstack::pop(void) {
    if (listLen == 0)
        return -16384;
    list[listLen - 1].nextCont = nullptr;
    listLen--;
    resultat = list[listLen].value;
    return resultat;
}

int Mychainedstack::operator%(int mod) const {
    if (mod < 2) return -65536;
    return listLen % mod;
}

void Mychainedstack::clear() {
    for (int i = listLen; i > 0; i--) {
        listLen--;
    }
}

Mychainedstack::~Mychainedstack() {
    clear();
    delete[] list;
    list = nullptr;
}
